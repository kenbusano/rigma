/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  This method is taken from https://github.com/vercel/next.js/tree/canary/examples/with-mongodb
 */

import { MongoClient } from 'mongodb'

if (!process.env.MONGO_KEY) {
  throw new Error('Invalid/Missing environment variable: MONGO_KEY')
}

const url = process.env.MONGO_KEY
const options = {}

let client
let clientPromise: Promise<MongoClient>

if (process.env.NODE_ENV === 'development') {
  let globalWithMongo = global as typeof globalThis & {
    _mongoClientPromise?: Promise<MongoClient>
  }
  if (!globalWithMongo._mongoClientPromise) {
    client = new MongoClient(url, options)
    globalWithMongo._mongoClientPromise = client.connect()
  }
  clientPromise = globalWithMongo._mongoClientPromise
} else {
  // In production mode, don't use a global variable.
  client = new MongoClient(url, options)
  clientPromise = client.connect()
}

export default clientPromise