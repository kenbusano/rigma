/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  I use other compilers for trying the new trend called virtual DOM.
 *  Upon "remotePattern" property this are the news portals that I preferred for now.
 */

import million from "million/compiler";

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**.inquirer.net",
        port: "",
        pathname: "/files/**",
      },
      {
        protocol: "https",
        hostname: "**.rappler.com",
        port: "",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: "philippines.mom-gmr.org",
        port: "",
        pathname: "/uploads/**",
      },
    ],
  },
  reactStrictMode: true,
};

export default million.next(nextConfig);