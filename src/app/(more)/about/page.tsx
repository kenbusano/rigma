/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

"use client";

import { Navbar } from "@/components/Navbar";
import { Fragment, useState, Suspense } from "react";
import Image from "next/image";
import { Roboto_Mono } from "next/font/google";
import Link from "next/link";
import { Footer } from "@/components/Footer";
import { SearchResult } from "@/components/Search";
import { ArtSkeleton } from "@/components/skeleton/Art";

//Doing my proper way of using optimize fonts from my love as hell
const codeSnippet = Roboto_Mono({
  subsets: ["latin"],
  variable: "--font-code",
});

export default function Page() {
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchQueryChange = (query: string) => {
    setSearchQuery(query);
  };

  return (
    <Fragment>
      <Navbar handleSearch={handleSearchQueryChange} />
      {searchQuery ? (
        <SearchResult searchQuery={searchQuery} />
      ) : (
        <Suspense fallback={<ArtSkeleton />}>
          <section className="lg:pt-12 p-4 lg:container lg:mx-auto px-6 lg:max-w-[768px] space-y-2 mb-6">
            <header className="space-y-3">
              <div id="bullet" className="space-y-2">
                <span className="sm:text-sm md:text-md lg:text-lg">
                  About Page
                </span>
                <h2 className="font-bold sm:text-xl lg:text-4xl break-normal">
                  This is just a hobby ok?
                </h2>
              </div>
              <div
                id="author"
                className="sm:space-x-2 lg:space-x-3 inline-flex items-center"
              >
                <Image
                  src="/images/marin.PNG"
                  width={1920}
                  height={1080}
                  className="sm:w-8 lg:w-12 sm:h-8 lg:h-12 rounded-full object-cover"
                  alt="eyelash"
                />
                <span className="sm:text-xs md:text-md lg:text-lg">
                  eyelash - May 31, 2023
                </span>
              </div>
            </header>
            <article className="break-normal space-y-3 sm:text-sm md:text-md lg:text-lg">
              <figcaption>
                So yeah, I develop this kind of project alone in just a hobby.
              </figcaption>
              <figcaption>
                In terms of things that I've done beginning with my first ever
                something big project named Astrid. So in these kind of project
                it contains my first ever web-designs that I've intiialize
                during Pandemic era (Starting from april 2020 I supposed)
                Therefore, I must admit personally for the fact that all kinds
                of my web designs that you might seen it has actually sort of
                bugs so I apologize because I use something way that I progress.
              </figcaption>
              <figcaption>
                This is the thing that I'm referring for I'm still have no idea
                to update some of those but I hope you still like it.
              </figcaption>
              <figure
                className={`bg-blue-950 p-3 px-3 rounded-lg ${codeSnippet.className} text-lime-300 sm:text-base md:text-md`}
              >
                <Link
                  href="https://astrid.netlify.app/"
                  target="_blank"
                  className="hover:underline"
                >
                  https://astrid.netlify.app/
                </Link>
              </figure>
              <figcaption>
                As telling my progress, from writing many indexes page in just
                HTML and styling with CSS upto the point that I'm now
                experiencing to deal with Web Frameworks I'm very happy for
                myself that all times that I've spent for studying just a web is
                like something needs to be done so I started to bring myself
                together to explore deep sides about technology in 2018 era.
              </figcaption>
              <figcaption>
                During 2019 era, the only thing that I wanted to learn is CSS
                but it takes so hard for me to do so because personally we have
                no computers back then, the only option I will do during that
                time is to stay in my school in our computer laboratory, This is
                actually kind of disturbing for teachers in some part when
                there's student idle around their prescence. In takes several
                years for me to learn only CSS even we have our basic web
                technology class so I've glad that I will use some of those
                lectures as my reference to explore more about CSS that time.
              </figcaption>
              <figcaption>
                So yeah, after several years I've just waiting for time that we
                could bought our new computers so this is the beginning of hell
                era (joke this is Pandemic) some of my unforgettable story
                actually begins in these pandemic. After all, I'm very happy
                that all time that I've spent is something like I've shown my
                personal grow, Looking forward to my future and sort of projects
                that needs to be done. So yeah, I'm very happy for myself that I
                was able to achieve this all alone so I'm hoping to look forward
                in your aspirations.
              </figcaption>
            </article>
          </section>
          <Footer />
        </Suspense>
      )}
    </Fragment>
  );
}
