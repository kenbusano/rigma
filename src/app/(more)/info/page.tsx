/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

"use client";

import { Navbar } from "@/components/Navbar";
import { Fragment, useState, Suspense } from "react";
import Image from "next/image";
import Link from "next/link";
import { Footer } from "@/components/Footer";
import { SearchResult } from "@/components/Search";
import { ArtSkeleton } from "@/components/skeleton/Art";


export default function Page() {
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchQueryChange = (query: string) => {
    setSearchQuery(query);
  };
  return (
    <Fragment>
      <Navbar handleSearch={handleSearchQueryChange} />
      {searchQuery ? (
        <SearchResult searchQuery={searchQuery} />
      ) : (
        <Suspense fallback={<ArtSkeleton />}>
          <section className="lg:pt-12 p-4 lg:container lg:mx-auto px-6 lg:max-w-[768px] space-y-2 mb-6">
            <header className="space-y-3">
              <div id="bullet" className="space-y-2">
                <span className="text-md">Some infos upon project</span>
                <h2 className="font-bold sm:text-xl lg:text-4xl break-normal">
                  Behind the bars
                </h2>
              </div>
              <div
                id="author"
                className="sm:space-x-2 lg:space-x-3 inline-flex items-center"
              >
                <Image
                  src="/images/marin.PNG"
                  width={1920}
                  height={1080}
                  className="sm:w-8 lg:w-12 sm:h-8 lg:h-12 rounded-full object-cover"
                  alt="eyelash"
                />
                <span className="sm:text-xs md:text-md lg:text-lg">
                  eyelash - June 01, 2023
                </span>
              </div>
            </header>
            <article className="break-normal space-y-3 sm:text-sm md:text-md lg:text-lg">
              <figcaption>
                In this project I used React to make these happen. For more
                details about the web technology stacks that I use you may check
                also
              </figcaption>
              <figure className="grid sm:grid-cols-1 md:grid-cols-2 gap-6">
                <Link
                  href="https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html"
                  target="_blank"
                  className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
                >
                  <div className="">
                    <Image
                      src="/svgs/typescript.svg"
                      width={1920}
                      height={1080}
                      className="w-full h-[120px] object-cover"
                      alt=""
                    />
                  </div>
                  <div className="p-4 space-y-2">
                    <h2 className="font-bold text-lg break-normal">
                      TypeScript
                    </h2>
                    <p className="text-sm">
                      My chosen language to be written still practicing to
                      explain and understand the OOP concepts.
                    </p>
                  </div>
                </Link>
                <Link
                  href="https://nextjs.org/docs/pages/building-your-application/upgrading/app-router-migration#step-6-migrating-data-fetching-methods"
                  target="_blank"
                  className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
                >
                  <div className="">
                    <Image
                      src="/svgs/nextJS.svg"
                      width={1920}
                      height={1080}
                      className="w-full h-[120px] object-cover"
                      alt=""
                    />
                  </div>
                  <div className="p-4 space-y-2">
                    <h2 className="font-bold text-lg break-normal">Next</h2>
                    <p className="text-sm">
                      I use version 13 because I love the
                      way they improve the data fetching check this out.
                    </p>
                  </div>
                </Link>
                <Link
                  href="https://tailwindcss.com/docs/browser-support"
                  target="_blank"
                  className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
                >
                  <div className="">
                    <Image
                      src="/svgs/tailwindCSS.svg"
                      width={1920}
                      height={1080}
                      className="w-full h-[120px] object-cover"
                      alt=""
                    />
                  </div>
                  <div className="p-4 space-y-2">
                    <h2 className="font-bold text-lg break-normal">Tailwind</h2>
                    <p className="text-sm">
                      Bitch, why am I too obsess to use this kind? Idk,
                      sometimes I can't assure this in terms of browser support
                      or maybe I'm just stupid.
                    </p>
                  </div>
                </Link>
                <Link
                  href="https://youtu.be/cC6HFd1zcbo?t=1042"
                  target="_blank"
                  className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
                >
                  <div className="">
                    <Image
                      src="/svgs/mongoDB.svg"
                      width={1920}
                      height={1080}
                      className="w-full h-[120px] object-cover"
                      alt=""
                    />
                  </div>
                  <div className="p-4 space-y-2">
                    <h2 className="font-bold text-lg break-normal">MongoDB</h2>
                    <p className="text-sm">
                      I saw something weird about the non-relational despite I
                      knew in the first place that Non-relational is the future
                      so would it be good to know your stand check this out.
                    </p>
                  </div>
                </Link>
              </figure>
            </article>
          </section>
        </Suspense>
      )}
      <Footer />
    </Fragment>
  );
}
