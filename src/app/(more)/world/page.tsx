/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  I use NewsAPI to fetch google news headlines.
 */

"use client";

import { Fragment, useEffect, useState, Suspense } from "react";
import Link from "next/link";
import { Navbar } from "@/components/Navbar";
import { CgDanger } from "react-icons/cg";
import { VscClose } from "react-icons/vsc";
import { GoogleNewsArticle } from "@/app/api/google-news/route";
import { SearchResult } from "@/components/Search";
import { SingleGrid } from "@/components/skeleton/SingleGrid";

export default function World() {
  const [artContent, setArtContent] = useState<GoogleNewsArticle[]>([]);
  const [showSection, setShowSection] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchQueryChange = (query: string) => {
    setSearchQuery(query);
  };

  const handleCloseClick = () => {
    setShowSection(false); //this is for close toggle modal
    sessionStorage.setItem("showNotice", "false");
  };

  useEffect(() => {
    fetch("https://rigma-one.vercel.app/api/google-news/", { next: { revalidate: 60 } })
      .then((response) => response.json())
      .then((data) => setArtContent(data.articles))
      .catch((error) => console.error(error));
  
    const storedValue = sessionStorage.getItem("showNotice");
    if (storedValue === "false") {
      setShowSection(false);
    }
  }, []);

  return (
    <Fragment>
      <Navbar handleSearch={handleSearchQueryChange} />
      {searchQuery ? (
        <SearchResult searchQuery={searchQuery} />
      ) : (
        <Suspense fallback={<SingleGrid />}>
          <section className="lg:pt-12 p-4 sm:space-y-2 lg:space-y-4 md:max-w-[768px] md:container md:mx-auto px-6">
            <div className="space-y-2">
              <h2 className="font-bold sm:text-xl lg:text-2xl">
                World Headlines
              </h2>
              <p className="sm:text-sm lg:text-md">
                Be updated on what's happening around the world.
              </p>
            </div>
            <aside className="grid sm:grid-cols-1 sm:gap-3 md:gap-4 lg:gap-6 mb-6">
              {artContent.map((headline) => (
                <div
                  className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
                  key={headline.source?.id || ""}
                >
                  <Link
                    href={headline.url || ""}
                    id="news-article"
                    target="_blank"
                  >
                    <div className="p-4 space-y-2 order-1">
                      <span className="text-sm">Google News</span>
                      <h2 className="font-bold sm:text-sm lg:text-lg truncate">
                        {headline.title}
                      </h2>
                      <p className="text-sm">
                        {new Date(headline.publishedAt || "").toDateString()}
                      </p>
                    </div>
                  </Link>
                </div>
              ))}
            </aside>
          </section>
        </Suspense>
      )}
      {showSection && (
        <section className="fixed sm:bottom-6 sm:right-3 md:bottom-6 md:right-6 rounded-lg bg-gradient-to-r from-violet-500 to-fuchsia-500 p-4 text-white">
          <div className="flex items-center space-x-2">
            <CgDanger className="lg:text-2xl" />
            <span className="sm:text-xs lg:text-[16px]">
              This page uses Google News API.
            </span>
            <VscClose
              className="lg:text-2xl hover:bg-gray-100 hover:text-black rounded-full cursor-pointer"
              onClick={handleCloseClick}
            />
          </div>
        </section>
      )}
    </Fragment>
  );
}
