/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  Simple explanation upon sorting arrays that I used for making one field in descending order modern
 *  languages like JS/TS has an built sorting arrays but in terms of what kind of sortings that I
 *  initialize here it's hard to tell what it is since it is built by default.
 */

import clientPromise from "@lib/mongodb";
import { NextResponse } from "next/server";
import { ArticleDB } from "types/articles";

export async function GET() {
  try {
    const client = await clientPromise;
    const db = client.db("rigma");
    const arts: ArticleDB[] = await db
      .collection<ArticleDB>("article")
      .find({})
      .sort({ article_date: -1 })
      .toArray();

    return NextResponse.json(arts, {
      status: 200,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type, Authorization",
      },
    });
  } catch (error) {
    console.error("An error occurred:", error);
  }
}
