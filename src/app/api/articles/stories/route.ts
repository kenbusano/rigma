/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 */

import clientPromise from "@lib/mongodb";
import { ArticleDB } from "types/articles";
import { NextResponse } from "next/server";

export async function GET() {
  try {
    const client = await clientPromise;
    const db = client.db("rigma");
    const arts: ArticleDB[] = await db
      .collection<ArticleDB>("topstories")
      .find({})
      .sort({ article_date: -1 })
      .toArray();

    return NextResponse.json(arts, {
      status: 200,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type, Authorization",
      },
    });
  } catch (error) {
    console.error("An error occurred:", error);
  }
}
