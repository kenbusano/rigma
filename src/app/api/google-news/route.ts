/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  I use NewsAPI for fetching google news data ^^
 */

import { NextResponse } from "next/server";

export interface GoogleNewsArticle {
  source?: {
    id: string;
    name: string;
  };
  author?: string | null;
  title?: string;
  description?: string;
  url?: string;
  urlToImage?: string | null;
  publishedAt?: Date;
  content?: string;
}

export async function GET() {
  const res = await fetch(process.env.RIGMA_APIKEY || "");
  const data: GoogleNewsArticle = (await res.json()) as GoogleNewsArticle;

  return NextResponse.json(data, {
    status: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers": "Content-Type, Authorization",
    },
  });
}
