/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  A dynamic metadata as I taught this was same for my way of data fetching.
 */

import { Metadata } from "next";
import { ObjectId } from "mongodb";
import { ArticleDB } from "types/articles";

export async function generateMetadata({
  params,
}: {
  params: { slug: ObjectId };
}): Promise<Metadata> {
  const { slug } = params;

  const [articles, fyp, stories] = await Promise.all([
    fetch("https://rigma-one.vercel.app/api/articles/").then((response) =>
      response.json()
    ),
    fetch("https://rigma-one.vercel.app/api/articles/fyp/").then((response) =>
      response.json()
    ),
    fetch("https://rigma-one.vercel.app/api/articles/stories/").then(
      (response) => response.json()
    ),
  ]);

  const mergedData: ArticleDB[] = [...articles, ...fyp, ...stories];
  const article: ArticleDB | undefined = mergedData.find(
    (item) => item._id === slug
  );

  if (!article) {
    return {
      title: "Article Not Found",
      description: "The requested article could not be found.",
    };
  }

  const {
    article_title,
    article_info,
    article_image,
    ...otherArticleProperties
  } = article;

  return {
    title: article_title,
    description: "Take some time to read this article from Rigma platform.",
    openGraph: {
      type: "article",
      title: article_title,
      description: "Take some time to read this article from Rigma platform.",
      siteName: 'Rigma | News Portal',
      images: [
        {
          url: article_image || "",
          alt: article_title,
        },
      ],
    },
    twitter: {
      card: "summary_large_image",
      title: article_title,
      description: article_info,
      images: [
        {
          url: article_image || "",
          alt: article_title,
        },
      ],
    },
    ...otherArticleProperties,
  };
}

export default function ArticleLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <section>{children}</section>;
}
