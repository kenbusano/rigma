/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  My demonstration of doing data fetch here I initialize the useState to assert my interface
 *  on the other hand the find array is solution.
 */

"use client";

import { Navbar } from "@/components/Navbar";
import { Fragment, useEffect, useState } from "react";
import Image from "next/image";
import { Footer } from "@/components/Footer";
import { ArticleDB } from "types/articles";
import { ObjectId } from "mongodb";
import { SearchResult } from "@/components/Search";
import { ArtSkeleton } from "@/components/skeleton/Art";
import Error from "@/app/error";

export default function NewsArt({ params }: { params: { slug: ObjectId } }) {
  //Search Query
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchQueryChange = (query: string) => {
    setSearchQuery(query);
  };

  //Fetch many API endpoints into single dynamic page ^^

  const [article, setArticle] = useState<ArticleDB[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
    //rigma-one.vercel.app
    Promise.all([
      fetch("https://rigma-one.vercel.app/api/articles/", {
        next: { revalidate: 60 },
      }).then((response) => response.json()),
      fetch("https://rigma-one.vercel.app/api/articles/fyp/", {
        next: { revalidate: 60 },
      }).then((response) => response.json()),
      fetch("https://rigma-one.vercel.app/api/articles/stories/", {
        next: { revalidate: 60 },
      }).then((response) => response.json()),
    ])
      .then(([articles, fyp, stories]) => {
        // Merge the results from all three API endpoints
        const mergedArticles = [...articles, ...fyp, ...stories];
        setArticle(mergedArticles);
      })
      .catch((error) => console.error(error));
  }, []);

  const art = article.find((item) => item._id === params.slug);

  //This will help if some of the value pairs are being null
  if (!art) {
    return null;
  }

  return (
    <Fragment>
      <Navbar handleSearch={handleSearchQueryChange} />
      {searchQuery ? (
        <SearchResult searchQuery={searchQuery} />
      ) : isLoading ? (
        <ArtSkeleton />
      ) : art ? (
        <section className="lg:pt-12 p-4 md:container md:mx-auto px-6 md:max-w-[768px] space-y-2 mb-6">
          <header className="space-y-3">
            <div id="bullet" className="space-y-2">
              <span className="lg:text-md">{art.article_topic}</span>
              <h2 className="font-bold sm:text-xl lg:text-4xl break-normal">
                {art.article_title}
              </h2>
            </div>
            <div id="author" className="space-x-3 inline-flex items-center">
              <Image
                src={art?.article_author_img || ""}
                width={1920}
                height={1080}
                className="sm:w-8 lg:w-12 sm:h-8 lg:h-12 rounded-full object-cover"
                alt="Image banner"
              />
              <span className="sm:text-xs md:text-md lg:text-lg">{`${
                art.article_author
              } - ${new Date(art?.article_date || "").toDateString()}`}</span>
            </div>
          </header>
          <article className="break-normal space-y-3 sm:text-sm md:text-md lg:text-lg">
            <figure>
              <Image
                src={`${art?.article_image}`}
                width={1920}
                height={1080}
                className="w-full sm:h-[140px] md:h-[200px] lg:h-[320px] rounded-lg object-cover"
                alt=""
              />
            </figure>
            <figcaption>{art.article_info}</figcaption>
          </article>
        </section>
      ) : (
        <Error />
      )}
      <Footer />
    </Fragment>
  );
}
