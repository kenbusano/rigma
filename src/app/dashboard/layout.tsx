/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *  
 */

import { Metadata } from "next/types";
import { Outfit } from "next/font/google";

//This stores for nested operation
const dashboardFont = Outfit({
  subsets: ["latin"],
  fallback: ["system-ui", "arial"],
});

export const metadata: Metadata = {
  title: "rigmauth | admin",
  description: "This operation is for authorize users only.",
  generator: "Next.js",
  applicationName: "Rigma",
  referrer: "origin-when-cross-origin",
  creator: "Kenneth Obsequio",
  icons: "./favicon.ico",
  formatDetection: {
    email: false,
    address: false,
    telephone: false,
  },
  /* SEO things starts at metadataBase */
  metadataBase: new URL("https://www.rigma-one.vercel.app/"),
  alternates: {
    canonical: "/",
    languages: {
      "en-US": "/en-US",
    },
  },
  openGraph: {
    title: "Rigma | News Portal",
    description: "Rigma is an independent news portal based on Philippines",
    url: "https://rigma-one.vercel.app/",
    siteName: "Rigma | News Portal",
    images: [
      {
        url: "https://rigma-one.vercel.app/images/rigma-og.png",
        width: 1200,
        height: 630,
      },
    ],
  },
  twitter: {
    card: "summary_large_image",
    title: "Rigma | News Portal",
    description: "Rigma is an independent news portal based on Philippines",
    images: "https://rigma-one.vercel.app/images/rigma-twitter.png",
  },
};

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <section className={`${dashboardFont.className} flex p-8 gap-8`}>
      {children}
    </section>
  );
}
