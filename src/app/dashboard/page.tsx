/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  The dashboard for admin (test mode)...
 */

"use client";

import { Artboard } from "@/components/dashboard/Artboard";
import { Sidebar } from "@/components/dashboard/Sidebar";
import { Fragment } from "react";

export default function Dashboard() {
  return (
    <Fragment>
      <Sidebar />
      <Artboard />
    </Fragment>
  );
}
