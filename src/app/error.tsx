/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

"use client";

import { Navbar } from "@/components/Navbar";
import { Fragment } from "react";

export default function Error() {
  return (
    <Fragment>
      <Navbar />
      <section className="lg:pt-12 p-4 sm:space-y-2 lg:space-y-4 md:max-w-[1024px] md:container md:mx-auto px-6">
        <div className="space-y-2">
          <h2 className="font-bold sm:text-xl lg:text-2xl">Error</h2>
          <p className="sm:text-sm lg:text-md">
            There's something wrong with the page you're looking at.
          </p>
        </div>
      </section>
    </Fragment>
  );
}
