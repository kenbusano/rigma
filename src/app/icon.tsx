/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

import { ImageResponse } from "next/server";

interface FavSize {
  width: number;
  height: number;
}

type ContentImage = "image/png";

export const runtime = ["edge"];

export const size: FavSize = {
  width: 32,
  height: 32,
};

export const contentType: ContentImage = "image/png";

export default function Icon() {
  return new ImageResponse(
    (
      <div
        style={{
          fontSize: 16,
          background: "brown",
          width: "100%",
          height: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: "100%",
          color: "white",
        }}
      >
        R
      </div>
    ),
    size
  );
}
