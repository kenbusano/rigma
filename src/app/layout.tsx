/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

import "./styles/globals.css";
import { Metadata } from "next/types";
import { Inter } from "next/font/google";

//This stores as the root font optimization
const defaultFont = Inter({
  subsets: ["latin"],
  fallback: ['system-ui', 'arial']
});

export const metadata: Metadata = {
  title: "Rigma - News Portal",
  description: "Rigma is an independent news portal based on Philippines",
  generator: "Next.js",
  applicationName: "Rigma",
  referrer: "origin-when-cross-origin",
  keywords: [
    "NextJS",
    "React",
    "TypeScript",
    "News Portal",
    "PH news portal",
    "Tailwind",
    "Web 2.0",
    "Rigma",
    "Open-Source",
    "MongoDB",
    "Rest API",
    "#RejectMarcosDuterte",
    "#MarcosMamatayTao",
    "#MarcosNotMyPresident",
    "#NeverAgain",
    "#NoToFascistRegime"
  ],
  creator: "Kenneth Obsequio",
  icons: "./favicon.ico",
  formatDetection: {
    email: false,
    address: false,
    telephone: false,
  },
  /* SEO things starts at metadataBase */
  metadataBase: new URL('https://www.rigma-one.vercel.app/'),
  alternates: {
    canonical: '/',
    languages: {
      'en-US': '/en-US',
    },
  },
  openGraph: {
    title: "Rigma | News Portal",
    description: "Rigma is an independent news portal based on Philippines",
    url: "https://rigma-one.vercel.app/",
    siteName: 'Rigma | News Portal',
    images: [
      {
        url: 'https://rigma-one.vercel.app/images/rigma-og.png',
        width: 1200,
        height: 630,
      },
    ]
  }, 
  twitter: {
    card: 'summary_large_image',
    title: "Rigma | News Portal",
    description: "Rigma is an independent news portal based on Philippines",
    images: 'https://rigma-one.vercel.app/images/rigma-twitter.png',
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={defaultFont.className}>{children}</body>
    </html>
  );
}
