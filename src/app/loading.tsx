/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  My loading screen simplified by framework so arigatooo!
 */

"use client";

import { Fragment, useState } from "react";
import { Navbar } from "@/components/Navbar";
import { Default } from "@/components/skeleton/Default";

export default function Loading() {
    const [searchQuery, setSearchQuery] = useState(null);

    const handleSearchQueryChange = (query: string) => {
      setSearchQuery(null);
    };

    return (
        <Fragment>
            <Navbar handleSearch={handleSearchQueryChange} />
            <Default />
        </Fragment>
    )
}