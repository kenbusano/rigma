/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 */
import { Metadata } from "next/types";

export const metadata: Metadata = {
  title: "Rigma - Login",
  description: "This operation is for authorize users only.",
  generator: "Next.js",
  applicationName: "Rigma",
  referrer: "origin-when-cross-origin",
  creator: "Kenneth Obsequio",
  icons: "./favicon.ico",
  formatDetection: {
    email: false,
    address: false,
    telephone: false,
  },
  /* SEO things starts at metadataBase */
  metadataBase: new URL("https://www.rigma-one.vercel.app/login"),
  alternates: {
    canonical: "/",
    languages: {
      "en-US": "/en-US",
    },
  },
  openGraph: {
    title: "Rigma | Login",
    description: "This page is for authorize users only.",
    url: "https://rigma-one.vercel.app/login",
    siteName: "Rigma | Login",
    images: [
      {
        url: "https://rigma-one.vercel.app/images/auth-only.png",
        width: 1200,
        height: 630,
      },
    ],
  },
  twitter: {
    card: "summary_large_image",
    title: "Rigma | Login",
    description: "This page is for authorize users only.",
    images: "https://rigma-one.vercel.app/images/auth-only.png",
  },
};

export default function LoginLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <section>{children}</section>;
}
