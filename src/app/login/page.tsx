/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  Admin page starts here...
 */

"use client";

import { Topbar } from "@/components/auth/Topbar";
import { Fragment, useState, useEffect } from "react";
import { useRouter } from "next/navigation"; //next13 applicable

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setError] = useState(false);
  const router = useRouter();

  const handleLogin = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (email === "admin@webhook.xyz" && password === "test") {
      setError(false);
      router.push("/dashboard");
      console.log("Successfully logged in!");
    } else {
      setError(true);
      console.log("Wrong credentials");
    }
  };

  //NextAuth library might be use after dashboard initialize commit ^^

  // const handleLogin = async (event: React.FormEvent<HTMLFormElement>) => {
  //   event.preventDefault();

  //   const result = await signIn("credentials", {
  //     email,
  //     password,
  //     redirect: true,
  //   });

  //   if (result?.error) {
  //     setError(true);
  //   }
  // };

  return (
    <Fragment>
      <Topbar />
      <aside className="md:container md:mx-auto p-6">
        <div className="flex flex-col items-center justify-center w-full space-y-3">
          <form onSubmit={handleLogin} className="space-y-4">
            {isError && (
              <div className="w-full sm:p-2 lg:p-4 pl-10 text-md rounded-lg bg-error font-bold text-md text-white text-center">
                Wrong credentials
              </div>
            )}
            <span className="font-bold text-5xl mb-4 text-center block">
              Log in
            </span>
            <div className="relative space-y-2">
              <label htmlFor="email">Email address</label>
              <input
                type="email"
                id="email"
                className="w-full sm:p-2 lg:p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-100 focus:outline-pinky"
                placeholder="Enter your email address..."
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </div>
            <div className="relative space-y-2">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                className="w-full sm:p-2 lg:p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-100 focus:outline-pinky"
                placeholder="Enter your password..."
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <div className="relative">
              <button
                type="submit"
                className="w-full sm:p-2 lg:p-4 pl-10 text-md rounded-lg bg-pinky font-bold text-md text-white hover:opacity-90"
              >
                Proceed
              </button>
            </div>
          </form>
        </div>
      </aside>
    </Fragment>
  );
}
