/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  Ternary operators is real as hell.
 */

"use client";

import { useState, Fragment, useEffect } from "react";
import { Articles } from "@/components/Content";
import { Navbar } from "@/components/Navbar";
import { SearchResult } from "@/components/Search";
import { Default } from "@/components/skeleton/Default";

export default function Home() {
  const [searchQuery, setSearchQuery] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  const handleSearchQueryChange = (query: string) => {
    setSearchQuery(query);
  };

  useEffect(() => {
    // Simulate a delay for loading data
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);

  return (
    <Fragment>
      <Navbar handleSearch={handleSearchQueryChange} />
      {searchQuery ? (
        <SearchResult searchQuery={searchQuery} />
      ) : (
        <Fragment>
          {isLoading ? <Default /> : <Articles />}
        </Fragment>
      )}
    </Fragment>
  );
}