/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

import { Fragment, useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { ArticleDB } from "types/articles";

export function Articles() {
  const [artContent, setArtContent] = useState<ArticleDB[]>([]);

  useEffect(() => {
    fetch("https://rigma-one.vercel.app/api/articles/", { next: { revalidate: 60 } })
      .then((response) => response.json())
      .then((data) => setArtContent(data))
      .catch((error) => console.error(error));
  }, []);

  return (
    <Fragment>
      <section className="lg:pt-12 p-4 sm:space-y-2 lg:space-y-4 md:max-w-[1024px] md:container md:mx-auto px-6">
        <div className="space-y-2">
          <h2 className="font-bold sm:text-xl lg:text-2xl">Headlines</h2>
          <p className="sm:text-sm lg:text-md">
            Be updated on what's happening right now
          </p>
        </div>
        <aside className="sm:columns-1 md:columns-2 space-y-4 mb-6">
          {artContent.map((headline) => (
            <div
              className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
              key={headline._id.toString()}
            >
              <Link href={`/blog/${headline._id}`} id="news-article">
                <figure>
                  <Image
                    src={headline.article_image || ""}
                    width={1920}
                    height={1080}
                    className="w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover"
                    alt="nothing here"
                    placeholder="blur"
                    blurDataURL={headline.article_image || ""}
                  />
                </figure>
                <div className="p-4 space-y-2">
                  <span className="text-sm">{headline.article_topic}</span>
                  <h2 className="font-bold sm:text-sm lg:text-lg break-normal">
                    {headline.article_title}
                  </h2>
                  <p className="text-sm">
                    {new Date(headline.article_date || "").toDateString()}
                  </p>
                </div>
              </Link>
            </div>
          ))}
        </aside>
      </section>
    </Fragment>
  );
}
