import Link from "next/link";

export function Footer() {
  return (
    <div className="border-t-2 border-gray-300 p-4 mb-4">
      <footer className="flex items-center justify-center container flex-col mx-auto space-y-3 text-xl text-center">
        <span className="font-bold">Get more news based on your interests</span>
        <Link
          href="/"
          className="p-2 px-6 bg-blue-700 text-white rounded-full hover:bg-blue-600"
        >
          Explore
        </Link>
      </footer>
    </div>
  );
}
