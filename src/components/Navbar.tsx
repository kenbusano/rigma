/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 */

import { Fragment, useState, useEffect, FormEvent } from "react";
import { FiMenu } from "react-icons/fi";
import Link from "next/link";
import { FiX } from "react-icons/fi";
import { BiWorld } from "react-icons/bi";
import { BsPersonFill, BsInfoCircleFill } from "react-icons/bs";
import { IoMdCloseCircle } from "react-icons/io";

import { GiWorld } from "react-icons/gi";

export function Navbar({
  handleSearch,
}: {
  handleSearch?: (query: string) => void;
}) {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const [showSection, setShowSection] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const query = event.target.value;
    setSearchQuery(query);
    if (handleSearch) {
      handleSearch(query);
    }
  };

  const handleFormSubmit = (event: FormEvent) => {
    event.preventDefault(); //prevent page refresh
  };

  const handleCloseClick = () => {
    setShowSection(false); //this is for close toggle modal
    localStorage.setItem("showSection", "false");
  };

  useEffect(() => {
    if (isSidebarOpen) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  
    const storedValue = sessionStorage.getItem("showSection");
    if (storedValue === "false") {
      setShowSection(false);
    }
  }, [isSidebarOpen]);

  return (
    <Fragment>
      {showSection && (
        <section className="sticky top-0 bg-gradient-to-r from-red-500 to-fuchsia-500 p-4 text-white">
          <div className="flex items-center justify-center space-x-2">
            <Link href="https://github.com/lash0000/lash/blob/master/BUGS.md" target="_blank" className="sm:text-md lg:text-[16px] hover:underline">
              This page has been abandoned for being stupid.
            </Link>
            <div className="sm:text-xl cursor-pointer">
              <IoMdCloseCircle onClick={handleCloseClick} />
            </div>
          </div>
        </section>
      )}
      <header className="bg-white sticky top-0 drop-shadow-xl p-3 z-20">
        <nav className="sm:px-0 md:px-4 lg:container lg:mx-auto flex items-center justify-between">
          <div
            id="wrapper"
            className="inline-flex items-center sm:space-x-1 lg:space-x-4"
          >
            <button
              id="menu-icon"
              className="sm:text-xl lg:text-2xl hover:bg-gray-100 rounded-full p-2"
              onClick={() => setIsSidebarOpen(!isSidebarOpen)}
            >
              <FiMenu />
            </button>
            <Link
              href="/"
              id="logo"
              className="font-bold sm:text-sm lg:text-xl"
            >
              Rigma | News
            </Link>
          </div>
          <form onSubmit={handleFormSubmit}>
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
            >
              Search
            </label>
            <div className="relative">
              <input
                type="search"
                id="default-search"
                className="block sm:w-[15ch] md:w-[32ch] lg:w-[50ch] sm:p-2 lg:p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-100 focus:outline-blue-700"
                placeholder="Search...."
                autoComplete="off"
                value={searchQuery}
                onChange={handleSearchInputChange}
              />
            </div>
          </form>
          <div id="rigma" className="space-x-2 sm:hidden lg:block">
            <Link
              href="/about"
              id="about-rigma"
              className="p-2 hover:bg-blue-700 hover:rounded-lg hover:text-white"
            >
              About
            </Link>
            <Link
              href="/info"
              id="info-app"
              className="p-2 hover:bg-blue-700 hover:rounded-lg hover:text-white"
            >
              Info
            </Link>
          </div>
        </nav>
      </header>
      {isSidebarOpen && (
        <div className="bg-black w-screen h-screen fixed top-0 bg-opacity-80 transition-opacity z-50">
          <aside
            className="bg-white h-full fixed top-0 left-0 w-80 overflow-auto"
            aria-label="Sidebar"
          >
            <div className="divide-y divide-gray-300">
              <div className="px-6 py-3 inline-flex items-center space-x-4">
                <button
                  id="menu-icon"
                  className="text-2xl hover:bg-gray-100 rounded-full p-2"
                >
                  <FiX
                    className="text-2xl"
                    onClick={() => setIsSidebarOpen(false)}
                  />
                </button>
                <span className="font-bold uppercase">Menu Tab</span>
              </div>
              <div className="px-6 py-6">
                <ul className="space-y-2">
                  <Link
                    href="/stories"
                    className="flex items-center p-2 text-gray-900 rounded-lg light:text-white hover:bg-blue-700 hover:text-white"
                  >
                    <BiWorld className="w-6 h-6" />
                    <span className="ml-3">Top stories</span>
                  </Link>
                  <Link
                    href="/fyp"
                    className="flex items-center p-2 text-gray-900 rounded-lg light:text-white hover:bg-blue-700 hover:text-white"
                  >
                    <BsPersonFill className="w-6 h-6" />
                    <span className="ml-3">For you</span>
                  </Link>
                  <Link
                    href="/world"
                    className="flex items-center p-2 text-gray-900 rounded-lg light:text-white hover:bg-blue-700 hover:text-white"
                  >
                    <GiWorld className="w-6 h-6" />
                    <span className="ml-3">World</span>
                  </Link>
                  <Link
                    href="/about"
                    className="flex items-center p-2 text-gray-900 rounded-lg light:text-white hover:bg-blue-700 hover:text-white lg:hidden"
                  >
                    <BsPersonFill className="w-6 h-6" />
                    <span className="ml-3">About</span>
                  </Link>
                  <Link
                    href="/info"
                    className="flex items-center p-2 text-gray-900 rounded-lg light:text-white hover:bg-blue-700 hover:text-white lg:hidden"
                  >
                    <BsInfoCircleFill className="w-6 h-6" />
                    <span className="ml-3">Info</span>
                  </Link>
                </ul>
              </div>
            </div>
          </aside>
        </div>
      )}
    </Fragment>
  );
}
