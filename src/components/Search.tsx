/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  My search engine tot
 */

import { Fragment, useEffect, useState } from "react";
import { ArticleDB } from "types/articles";
import Image from "next/image";
import Link from "next/link";
import { Default } from "./skeleton/Default";

export function SearchResult({ searchQuery }: { searchQuery: string }) {
  const [searchResults, setSearchResults] = useState<ArticleDB[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
    fetchSearchResults();
  }, [searchQuery]);

  const fetchSearchResults = async () => {
    try {
      const [articlesResponse, fypResponse, storiesResponse] =
        await Promise.all([
          fetch("/api/articles").then((response) => response.json()),
          fetch("/api/articles/fyp").then((response) => response.json()),
          fetch("/api/articles/stories").then((response) => response.json()),
        ]);
      const mergedResults = [
        ...articlesResponse,
        ...fypResponse,
        ...storiesResponse,
      ];
      setSearchResults(mergedResults);
    } catch (error) {
      console.error(error);
    }
  };

  const renderSearchResults = () => {
    const filteredResults = searchResults.filter((result) => {
      const query = searchQuery.toLowerCase();
      const title = result.article_title?.toLowerCase();
      const info = result.article_info?.toLowerCase();

      return title?.includes(query) || info?.includes(query);
    });

    if (filteredResults.length === 0) {
      return (
        <figure className="md:w-[594px] lg:w-[60rem] flex items-center justify-center">
          <Image
            src="/svgs/search-engine.svg"
            width={1920}
            height={1080}
            className="w-96 object-cover"
            alt="nothing here"
          />
        </figure>
      );
    } else {
      return filteredResults.map((result) => (
        <div
          className="border border-gray-300 rounded-lg hover:bg-blue-700 hover:text-white overflow-hidden"
          key={result._id.toString()}
          title={result.article_title}
        >
          <Link href={`/blog/${result._id}`} id="news-article">
            <figure>
              <Image
                src={result.article_image || ""}
                width={1920}
                height={1080}
                className="w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover"
                alt="nothing here"
                placeholder="blur"
                blurDataURL={result.article_image || ""}
              />
            </figure>
            <div className="p-4 space-y-2">
              <span className="text-sm">{result.article_topic}</span>
              <h2 className="font-bold sm:text-sm lg:text-lg break-normal">
                {result.article_title}
              </h2>
              <p className="text-sm">
                {new Date(result.article_date || "").toDateString()}
              </p>
            </div>
          </Link>
        </div>
      ));
    }
  };
  return (
    <Fragment>
      {isLoading ? (
        <Default />
      ) : (
        <section className="lg:pt-12 p-4 sm:space-y-2 lg:space-y-4 md:max-w-[1024px] md:container md:mx-auto px-6">
          <div className="space-y-2">
            <span className="sm:text-sm lg:text-md">Search results for</span>
            <h2 className="font-bold sm:text-xl lg:text-2xl">{searchQuery}</h2>
          </div>
          <aside className="sm:columns-1 md:columns-2 space-y-4 mb-6">
            {renderSearchResults()}
          </aside>
        </section>
      )}
    </Fragment>
  );
}
