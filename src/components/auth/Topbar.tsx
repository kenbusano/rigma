/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 */

import Link from "next/link";
import { Fragment } from "react";

export function Topbar() {
  return (
    <Fragment>
      <header className="bg-white sticky top-0 border-b-2 border-gray-300 p-6 mb-6 z-20">
        <nav className="sm:px-0 md:px-4 lg:container lg:mx-auto flex items-center justify-between">
          <Link href="/" id="logo" className="font-bold sm:text-sm lg:text-xl">
            Rigma | Login
          </Link>
          <Link href="" className="text-md">
            Details
          </Link>
        </nav>
      </header>
    </Fragment>
  );
}
