/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 */

import Image from "next/image";

export function Artboard() {
  return (
    <main className="flex flex-col gap-8 w-full">
      <header className="bg-white drop-shadow-lg p-4 pr-6 rounded-lg flex items-center justify-end">
        <div id="logo-user" className="order-2 ml-3">
          <Image
            src={"/images/marin.PNG"}
            width={64}
            height={64}
            className="w-12 h-12 object-cover rounded-full"
            alt="nothing here"
          />
        </div>
        <div className="order-1 text-right">
          <span className="text-xl font-bold">admin@webhook.xyz</span>
          <p>Maintainer</p>
        </div>
      </header>
      <aside className="rounded-lg grid grid-cols-2 gap-8">
        <div className="bg-red-500 p-4 rounded-lg">test me</div>
        <div className="bg-red-500 p-4 rounded-lg">test me</div>
      </aside>
    </main>
  );
}
