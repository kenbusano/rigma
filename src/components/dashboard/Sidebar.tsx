/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  The dashboard for admin (test mode)...
 */

import { FiMenu, FiLogOut } from "react-icons/fi";
import Link from "next/link";
import { BiHomeCircle, BiCategory } from "react-icons/bi";
import { RiSettingsLine } from "react-icons/ri";
import { useState } from "react";

export function Sidebar() {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  
  return (
    <nav className="bg-pink-500 p-6 sticky top-0 rounded-lg space-y-3 text-white">
      <div id="nav-link" className="flex flex-col space-y-1 h-[70vh]">
        <div id="logo" className="inline-flex items-center space-x-2 p-2 pb-6">
          <FiMenu className="text-2xl cursor-pointer" />
        </div>
        <Link
          href=""
          className="inline-flex items-center space-x-4 p-2 hover:bg-pink-400 rounded-lg"
        >
          <BiHomeCircle className="text-2xl" />
          <span className="text-md absolute left-14 rounded-lg bg-pink-400 p-2 w-28 hidden">
            Home
          </span>
        </Link>
        <Link
          href=""
          className="inline-flex items-center space-x-4 p-2 hover:bg-pink-400 rounded-lg"
        >
          <BiCategory className="text-2xl" />
          <span className="text-md absolute left-14 rounded-lg bg-pink-400 p-2 w-28 hidden">
            Articles
          </span>
        </Link>
      </div>
      <div id="nav-link" className="flex flex-col space-y-1">
        <Link
          href=""
          className="inline-flex items-center space-x-4 p-2 hover:bg-pink-400 rounded-lg"
        >
          <RiSettingsLine className="text-2xl" />
          <span className="text-md absolute left-14 rounded-lg bg-pink-400 p-2 w-28 hidden">
            Settings
          </span>
        </Link>
        <Link
          href=""
          className="inline-flex items-center space-x-4 p-2 hover:bg-pink-400 rounded-lg"
        >
          <FiLogOut className="text-2xl" />
          <span className="text-md absolute left-14 rounded-lg bg-pink-400 p-2 w-28 hidden">
            Log out
          </span>
        </Link>
      </div>
    </nav>
  );
}
