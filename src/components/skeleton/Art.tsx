/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  Skeleton load for dynamic blog page
 */

export function ArtSkeleton() {
  return (
    <section className="lg:pt-12 p-4 md:container md:mx-auto px-6 md:max-w-[768px] space-y-2 mb-6 animate-pulse">
      <header className="space-y-3">
        <div id="bullet" className="space-y-2">
          <div className="h-6 bg-gray-300 w-80 rounded-full" />
          <div className="h-6 bg-gray-300 w-56 rounded-full" />
        </div>
        <div id="author" className="space-x-3 inline-flex items-center">
          <div className="bg-gray-300 sm:w-8 lg:w-12 sm:h-8 lg:h-12 rounded-full object-cover" />
          <div className="h-3 bg-gray-300 w-56 rounded-full" />
        </div>
      </header>
      <article className="break-normal space-y-3 sm:text-sm md:text-md lg:text-lg">
        <figure>
          <div className="bg-gray-300 w-full sm:h-[140px] md:h-[200px] lg:h-[320px] rounded-lg object-cover" />
        </figure>
        <figcaption className="space-y-2">
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-32 rounded-full" />
        </figcaption>
        <figcaption className="space-y-2">
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-32 rounded-full" />
        </figcaption>
        <figcaption className="space-y-2">
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-full rounded-full" />
          <div className="h-3 bg-gray-300 w-32 rounded-full" />
        </figcaption>
      </article>
    </section>
  );
}
