/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  My loading skeletion nextJS provides better thing.
 */

export function Default() {
  return (
    <section className="lg:pt-12 p-4 sm:space-y-2 lg:space-y-4 md:max-w-[1024px] md:container md:mx-auto px-6">
      <div className="animate-pulse space-y-3">
        <div className="h-6 bg-gray-300 w-80 rounded-full" />
        <div className="h-6 bg-gray-300 w-56 rounded-full" />
      </div>
      <aside className="grid sm:grid-cols-1 md:grid-cols-2 sm:gap-3 md:gap-4 lg:gap-6 mb-6">
        <div className="border border-gray-300 rounded-lg overflow-hidden">
          <div className="animate-pulse">
            <figure>
              <div className="bg-gray-300 w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover" />
            </figure>
            <div className="p-4 space-y-3">
              <div className="h-3 bg-gray-300 w-56 rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-32 rounded-full" />
            </div>
          </div>
        </div>
        <div className="border border-gray-300 rounded-lg overflow-hidden">
          <div className="animate-pulse">
            <figure>
              <div className="bg-gray-300 w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover" />
            </figure>
            <div className="p-4 space-y-3">
              <div className="h-3 bg-gray-300 w-56 rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-32 rounded-full" />
            </div>
          </div>
        </div>
        <div className="border border-gray-300 rounded-lg overflow-hidden">
          <div className="animate-pulse">
            <figure>
              <div className="bg-gray-300 w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover" />
            </figure>
            <div className="p-4 space-y-3">
              <div className="h-3 bg-gray-300 w-56 rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-32 rounded-full" />
            </div>
          </div>
        </div>
        <div className="border border-gray-300 rounded-lg overflow-hidden">
          <div className="animate-pulse">
            <figure>
              <div className="bg-gray-300 w-full sm:h-[120px] md:h-[160px] lg:h-[200px] object-cover" />
            </figure>
            <div className="p-4 space-y-3">
              <div className="h-3 bg-gray-300 w-56 rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-full rounded-full" />
              <div className="h-3 bg-gray-300 w-32 rounded-full" />
            </div>
          </div>
        </div>
      </aside>
    </section>
  );
}
