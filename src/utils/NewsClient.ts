/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  This is my unused test data I use my database ofcourse but let me keep this one ^^
 */

// interface ArticleClient {
//     article_id: number,
//     article_author: string,
//     article_author_img: string,
//     article_topic: string,
//     article_date: Date,
//     article_title: string,
//     article_info: string,
//     article_image?: string,
//     article_embed?: string,
// }

// export const newsClient: ArticleClient[] = [
//     {
//         article_id: 1234567890,
//         article_author: "test",
//         article_author_img: "/images/person.svg",
//         article_date: new Date("05/22/2023"),
//         article_topic: "test",
//         article_title: "test",
//         article_image: "test",
//         article_info: "test."
//     },
// ]