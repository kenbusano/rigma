/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 * 
 *  This is what I wanted for tailwind check this out wohoo ^^
 *  https://tailwindcss.com/blog/tailwindcss-v3-3#esm-and-type-script-support
 * 
 */

import type { Config } from "tailwindcss";

export default {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      colors: {
        pinky: "#dd1c77",
        notice: "#78c679",
        error: "#e34a33",
        pinkylight: "#ff3d99",
      },
    },
    screens: {
      sm: "320px",
      md: "640px",
      lg: "1020px",
      xl: "1280px",
    },
  },
  plugins: [],
} satisfies Config;