/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  Global interfaces for client page and admin operation
 */

export interface ArticleDB {
  _id: ObjectId;
  article_author?: string;
  article_author_img?: string;
  article_topic?: string;
  article_date?: Date;
  article_title?: string;
  article_info?: string;
  article_image?: string;
  article_embed?: string;
}

export interface AdminDB {
  _id: ObjectId,
  rigma_usn?: string;
  rigma_email?: string;
  rigma_hash?: string | Hash;
}