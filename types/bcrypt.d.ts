/**
 *  2023 - DEV
 *  Developer: Kenneth Obsequio
 *  GitHub: https://github.com/kenbusano
 *  License: MIT
 *
 *  An outdated JS library needs to initialize a type definition
 */

declare module "bcryptjs" {
  export function compare(data: string, encrypted: string): Promise<boolean>;
}
